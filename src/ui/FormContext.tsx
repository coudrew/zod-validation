import { createContext, useState } from "react";
import { z } from "zod";

export type FormFieldValue = {
  value: string;
  message: string;
};

export type FormFields = {
  [key: string]: FormFieldValue;
};

export type FormContextValue = {
  fields: FormFields;
  formValid: boolean;
  setField?: React.FormEventHandler;
  onSubmit?: React.FormEventHandler;
};
const formDefault = {
  fields: {
    firstName: { value: "", message: "" },
    lastName: { value: "", message: "" },
    email: { value: "", message: "" },
    password: { value: "", message: "" },
  },
  formValid: false,
};

const formValidator: Record<string, any> = {
  firstName: z.string().min(1, { message: "First Name is required" }),
  lastName: z.string().min(1, { message: "Last Name is required" }),
  email: z
    .string()
    .min(1, { message: "Email is required" })
    .email({ message: "Invalid email address" }),
  password: z
    .string()
    .min(8, { message: "Password must be at least 8 characters" })
    .max(32, { message: "Password must be 32 characters or less" })
    .regex(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/g, {
      message:
        "Password must contain at least one lowercase letter, one uppercase letter, and one special character.",
    }),
};

const useFormCanSubmit = (formFields: FormFields, fields: string[] = []) => {
  let valid = false;
  let validation = true;
  for (const fieldKeyIndex in fields) {
    const { value, message } =
      formFields[fields[fieldKeyIndex] as keyof FormFields];
    validation = Boolean(value && !message);
    if (!validation) break;
  }
  if (validation !== valid) {
    valid = validation;
  }
  return valid;
};

export const FormContext = createContext<FormContextValue>(formDefault);

export const FormContextProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [formState, setFormState] = useState(formDefault);
  const setField = (event: React.FormEvent<HTMLInputElement>) => {
    const {
      currentTarget: { name, value },
    } = event;

    const result = formValidator[name].safeParse(value);
    setFormState({
      ...formState,
      fields: {
        ...formState.fields,
        [name]: {
          value: value,
          message: result.success ? "" : result.error.issues[0].message,
        },
      },
    });
  };

  const onSubmit = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    console.log("submit");
  };

  const formValid = useFormCanSubmit(formState.fields, [
    "firstName",
    "lastName",
    "email",
    "password",
  ]);

  const contextValue = {
    ...formState,
    formValid,
    setField,
    onSubmit,
  };
  return (
    <FormContext.Provider value={contextValue}>{children}</FormContext.Provider>
  );
};
