import clsx from "clsx";
import { FormFieldValue } from "./FormContext";
import { memo, useId } from "react";

const FormTextInput: React.FC<{
  setField: React.FormEventHandler | undefined;
  value: FormFieldValue;
  label: string;
  name: string;
  type?: string;
}> = ({ value, label, name, type = "text", setField }) => {
  const id = useId();
  return (
    <>
      <label htmlFor={id}>{label}</label>
      <input
        className={clsx("p-1 leading-5", {
          "outline-red-500": value.message,
        })}
        id={id}
        name={name}
        type={type}
        value={value.value}
        onChange={setField}
      />
      <label className="text-sm text-red-500">{value.message}</label>
    </>
  );
};

export default memo(FormTextInput);
