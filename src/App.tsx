import { useContext } from "react";
import { FormContext } from "./ui/FormContext";
import FormTextInput from "./ui/FormTextInput";

function App() {
  const {
    fields: { firstName, lastName, email, password },
    setField,
    onSubmit,
    formValid,
  } = useContext(FormContext);

  return (
    <div className="w-full h-full absolute bg-slate-800 flex flex-row justify-center items-center">
      <form
        className="w-1/2 h-3/4 relative bg-slate-300 rounded-md flex flex-col space-y-3 p-3"
        onSubmit={onSubmit}
      >
        <FormTextInput
          name="firstName"
          label="First Name"
          setField={setField}
          value={firstName}
        />
        <FormTextInput
          name="lastName"
          label="Last Name"
          setField={setField}
          value={lastName}
        />
        <FormTextInput
          name="email"
          label="Email"
          setField={setField}
          value={email}
          type="email"
        />
        <FormTextInput
          name="password"
          label="Password"
          setField={setField}
          value={password}
          type="password"
        />
        <button
          className={`${formValid ? "bg-green-400" : "bg-red-500"}`}
          type="submit"
          disabled={!formValid}
        >
          Submit
        </button>
      </form>
    </div>
  );
}

export default App;
